window.onload = function() {
    console.log('Dokument geladen');
    var s = document.createElement("script");
    s.src = ".jquery-3.4.1.min.js";
    s.onload = function(e){ console.log(geladen) };

};

$(document).on('ready', function () {
    $(".regular").slick({
        autoplay: false,
        autoplaySpeed: 2000,
        centerPadding: 0,
        dots: true,
        infinite: true,
        listWidth: 33,
        pauseOnHover: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    arrows: false,
                    slidesToShow: 3,
                    adaptiveHeight: true,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    arrow: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
        slidesToShow: 3,
        slidesToScroll: 3
    });
});

function includeHTML() {
    var z, i, elmnt, file, xhttp;
    /* Loop through a collection of all HTML elements: */
    z = document.getElementsByTagName("*");
    for (i = 0; i < z.length; i++) {
        elmnt = z[i];
        /*search for elements with a certain atrribute:*/
        file = elmnt.getAttribute("include-html");
        if (file) {
            /* Make an HTTP request using the attribute value as the file name: */
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4) {
                    if (this.status == 200) {elmnt.innerHTML = this.responseText;}
                    if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
                    /* Remove the attribute, and call this function once more: */
                    elmnt.removeAttribute("include-html");
                    includeHTML();
                }
            };
            xhttp.open("GET", file, true);
            xhttp.send();
            /* Exit the function: */
            return;
        }
    }
}
